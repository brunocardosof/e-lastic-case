<h1 align="center">
  E-lastic Case
</h1>

Case proposto pela empresa E-lastic para vaga de desenvolvedor react-native

<h2 align="left">
  Tecnologias usadas
</h2>

<h3> Mobile </h3>

React-native

<h2 align="left"> Como rodar o aplicativo em desenvolvimento: </h2>

```bash
$ MOBILE
$ na raiz do projeto:
$ npm i
$ npx react-native run-android
```

<h4>APK para teste:</h4>
https://drive.google.com/file/d/1XKtFCMcqtVW_oXE-O3_clDP2xtPwEGP1/view?usp=sharing
